// translator tool
const Ocp_Apim_Subscription_Key_Translation = ""; //Your-Subscription-Key-for-Translator
const Request_region_Translation = "";  //Your-Resource-Region-for-Translator 
const Request_URL_Translation = "https://api.cognitive.microsofttranslator.com/"; //endpoint-for-Translator 

// speech tool
const Ocp_Apim_Subscription_Key_Speech = ""; //Your-Subscription-Key-for-Speech 
const Request_region_Speech = "";  //Your-Region


// 單字出題	
// online text convert to array: https://arraythis.com/
var items = ["asleep", "absolute", "accept", "eclipse", "extend", "gratitude"];

var resultDiv;
var SpeechSDK;
var synthesizer;
var speechConfig;
var startSpeakTextAsyncButton;
var phrase;
var startRecognizeOnceAsyncButton;
var phraseDiv;
var speakans;
var score=0;
var number=0;
var dataArray = [];
var text ="";


document.addEventListener("DOMContentLoaded", function () {

	
    $("#action-button").click(function(){
		number +=1;
		$("#number").html(number);
		$('#translation-textarea').val("");
		$("#phraseDiv").val("");
		$("#startRecognizeOnceAsyncButton").attr("disabled", false);
		$("#translation-button").attr("disabled", false);
		
		
        
		text =""
		text = items[Math.floor(Math.random()*items.length)];
		console.log(text);
		console.log(dataArray);
		phrase = text
		$('#question-textarea').val(text);
		$("#action-button").attr("disabled", true);
		
        
		
		$("#translation-button").click(function(){
			var Language = 'zh-hant';
			dataArray.push({"text":text});
			$.ajax({
				url: Request_URL_Translation+"translate?api-version=3.0&to=" + Language,
				data: JSON.stringify(dataArray),
				type: "POST",
				dataType: "json",
				contentType: "application/json;charset=utf-8",
				headers : {
					'Ocp-Apim-Subscription-Key': Ocp_Apim_Subscription_Key_Translation,
					'Ocp-Apim-Subscription-Region': Request_region_Translation,
				},
				success: function(result){
					result.forEach(data => {
						data["translations"].forEach(translation => {
							$('#translation-textarea').val(translation['text']);
						});
					});
				},
				error: function(xhr, ajaxOptions, thrownError){
					console.log(xhr.status);
					console.log(ajaxOptions);
					console.log(thrownError);
				}
			});
			$("#translation-button").attr("disabled", true);
		
		});
	});
		
		

		startSpeakTextAsyncButton = $("#startSpeakTextAsyncButton");
		resultDiv = $("#resultDiv");
		$("#startSpeakTextAsyncButton").click(function(){
			startSpeakTextAsyncButton.disabled = true;
			
			
		if (Ocp_Apim_Subscription_Key_Speech === "" || Ocp_Apim_Subscription_Key_Speech === "subscription") {
            alert("Please enter your Microsoft Cognitive Services Speech subscription key!");
            startSpeakTextAsyncButton.disabled = false;
            return;
        }
        var speechConfig = SpeechSDK.SpeechConfig.fromSubscription(Ocp_Apim_Subscription_Key_Speech, Request_region_Speech);

        speechConfig.speechSynthesisLanguage = 'en-US';

        synthesizer = new SpeechSDK.SpeechSynthesizer(speechConfig);

        let inputText = phrase;
        synthesizer.speakTextAsync(
            inputText,
            function (result) {
                startSpeakTextAsyncButton.disabled = false;
                if (result.reason === SpeechSDK.ResultReason.SynthesizingAudioCompleted) {
                    resultDiv.innerHTML += "synthesis finished for [" + inputText + "].\n";
                } else if (result.reason === SpeechSDK.ResultReason.Canceled) {
                    resultDiv.innerHTML += "synthesis failed. Error detail: " + result.errorDetails + "\n";
                }
                window.console.log(result);
                synthesizer.close();
                synthesizer = undefined;
            },
            function (err) {
                startSpeakTextAsyncButton.disabled = false;
                resultDiv.innerHTML += "Error: ";
                resultDiv.innerHTML += err;
                resultDiv.innerHTML += "\n";
                window.console.log(err);

                synthesizer.close();
                synthesizer = undefined;
            });
		});

    if (!!window.SpeechSDK) {
        SpeechSDK = window.SpeechSDK;
        startSpeakTextAsyncButton.disabled = false;

        // in case we have a function for getting an authorization token, call it.
        if (typeof RequestAuthorizationToken === "function") {
            RequestAuthorizationToken();
        }
    }
		
		
	
	
	startRecognizeOnceAsyncButton =$("#startRecognizeOnceAsyncButton");
	$("#startRecognizeOnceAsyncButton").click(function(){
		startRecognizeOnceAsyncButton.disabled = true;
		$("#phraseDiv").val("");
        if (Ocp_Apim_Subscription_Key_Speech === "" || Ocp_Apim_Subscription_Key_Speech === "subscription") {
            alert("Please enter your Microsoft Cognitive Services Speech subscription key!");
            return;
        }
        var speechConfig = SpeechSDK.SpeechConfig.fromSubscription(Ocp_Apim_Subscription_Key_Speech, Request_region_Speech);

		speechConfig.speechRecognitionLanguage = 'en-US';
        var audioConfig = SpeechSDK.AudioConfig.fromDefaultMicrophoneInput();
        recognizer = new SpeechSDK.SpeechRecognizer(speechConfig, audioConfig);

        recognizer.recognizeOnceAsync(
            function (result) {
                startRecognizeOnceAsyncButton.disabled = false;
				speakans ="";
				speakans = result.text;				
				console.log(speakans);
				$("#phraseDiv").val(result.text);
                
                recognizer.close();	

				window.console.log(result);
                recognizer = undefined;
				
				
				speakans = speakans.toLocaleLowerCase();
				speakans=speakans.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\ |\=|\||\\|\[|\]|\{|\}|\;|\:|\”|\’|\,|\<|\.|\>|\/|\?]/g,'');
				console.log(speakans);
				if(speakans == phrase || result.text == phrase){
					alert("恭喜您挑戰成功!🙆‍再前往下一題挑戰");
					score +=10;
					$("#score").html(score);
					$("#action-button").attr("disabled", false);
					$("#startRecognizeOnceAsyncButton").attr("disabled", true);
					$("#action-button").html('下一題挑戰💪');
				}else{
					alert("聽不清楚，請再回答一次 ! 😲");
					score -=5;
					$("#score").html(score);
				}
				
				
            },
            function (err) {
                startRecognizeOnceAsyncButton.disabled = false;
				$("#phraseDiv").val(err);
                window.console.log(err);

                recognizer.close();
                recognizer = undefined;
            });
		

	});
		
		
		

   
});